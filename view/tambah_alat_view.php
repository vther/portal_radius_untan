<html>
<head>
	<title>Hotspot UNTAN</title>
	<link rel="stylesheet" href="css/tambah_alat.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/logged_in.js"></script>
</head>
<body>
	<div id="header">
		<img src="image/header-untan.png" height="80px"> Hotspot Universitas Tanjungpura
	</div>
	<div id="content">
		<div id="content_header">
			Selamat Datang
		</div>
		<div id="content_body">
			sistem kami mendeteksi bahwa anda baru pertama kali login ke hotpot UNTAN dengan perangkat yang sedang anda gunakan saat ini
			<form action="tambah_alat_proses.php" method="post">
				<input type="hidden" name="mac" value="<?php echo $mac ?>">
				<input type="hidden" name="username" value="<?php echo $username ?>">
				<input type="hidden" name="password" value="<?php echo $password ?>">
				<input type="hidden" name="status" value="<?php echo $status ?>">
				<input type="hidden" name="hostname" value="<?php echo $hostname ?>">
				<input type="hidden" name="new_user" value="<?php echo $new_user ?>">
				<div id="form_header">Silahkan beri nama untuk perangkat anda</div>
				<div id="form_input"><label for="">Nama Perangkat :</label><input type="text" id="nama_alat" placeholder="Cth (Laptop saya, android saya, dll)" name="nama_perangkat"><input id="submit" type="submit" value="Submit"></div>
			</form>
		</div>
	</div>
</body>
</html>