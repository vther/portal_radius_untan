<html>
<head>
	<title>Hotspot UNTAN</title>
	<link rel="stylesheet" href="css/logged_in.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/logged_in.js"></script>
	<script type="text/javascript">
		var link = '<?php echo $link; ?>';
	</script>
	<script type="text/javascript" src="http://<?php echo $hostname;?>/login_status.html"></script>
</head>
<body>
	<a id="pop" href="<?php echo $link;?>" target="_blank"></a>
	<div id="header">
		<img src="image/header-untan.png" height="80px"> Hotspot Universitas Tanjungpura
	</div>
	<div id="content">
		<div id="content_header">
			Selamat Datang <?php echo $_SESSION['username'];?>
			<a href="logout.php?hostname=<?php echo $hostname?>"><div id="logout">Log Out</div></a>
		</div>
		<div id="content_body">
			<div id="left">
				<img src="http://siakad.untan.ac.id/web_scripts/f_mhs_base64photo.php?nim=<?php echo $mahasiswa->data['username'];?>" alt="">
			</div>
			<div id="right">
				<label for="">Nama :</label><span><?php echo $mahasiswa->data['nama'];?></span>
				<label for="">NIM :</label><span><?php echo $mahasiswa->data['username'];?></span>
				<label for="">Program Studi :</label><span><?php echo $mahasiswa->data['progdi'];?></span>
				<label for="">IPS :</label><span><?php echo $mahasiswa->data['ips'];?></span>
				<label for="">IPK :</label><span><?php echo $mahasiswa->data['ipk'];?></span>
				<?php 
				$jml_alat = count($alat->alat_array);
				?>
				<table cellspacing="0px">
					<tr>
						<td class="table_header">Nama Alat</td>
						<td class="table_header">MAC</td>
						<?php 
						if($jml_alat > 1)
						{
						echo'<td class="table_header">Hapus</td>';
						}
						?>
					</tr>
					<?php
					$jml_alat = count($alat->alat_array);
					foreach($alat->alat_array as $alat)
					{
						echo'
						<tr>
							<td>'.$alat["device_name"].'</td>
							<td>'.$alat["mac"].'</td>';
						
						if($jml_alat > 1)
						{
							echo'
							<td><a href="hapus_alat.php?id='.$alat["id"].'">hapus</a></td>
							';
						}

						echo '</tr>';
					}
					?>
				</table>
			</div>
		</div>
	</div>
</body>
</html>
