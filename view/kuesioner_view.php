<html>
<head>
	<title>Kuesioner</title>
	<link rel="stylesheet" href="css/kuesioner.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/kuesioner.js"></script>
</head>
<body>
	<div id="header">
		<img src="image/header-untan.png" height="80px"> Hotspot Universitas Tanjungpura
	</div>
	<div id="content">
		<div id="content_header">
			Kuesioner tentang Jaringan Hotspot UNTAN
		</div>
		<div id="content_body">
				<ol>
					<form action="kuesioner_proses.php" method="post" id="form_kuesioner">
					<li>
						Seberapa sering anda menggunakan internet melalui hotspot UNTAN dalam 1 minggu ? <br>
						<input type="radio" name='1' value='a'>Setiap hari <br>
						<input type="radio" name='1' value='b'>Dua hari sekali <br>
						<input type="radio" name='1' value='c'>Tiga hari sekali <br>
						<input type="radio" name='1' value='d'>Seminggu sekali <br>
						<input type="radio" name='1' value='e'>Tidak pernah <br>
					</li>
					<li>
						Ketika sedang menggunakan hotspot di Untan, berapa lama anda mengaksesnya ? <br>
						<input type="radio" name='2' value='a'>10 Menit <br>
						<input type="radio" name='2' value='b'>30 Menit <br>
						<input type="radio" name='2' value='c'>1 Jam <br>
						<input type="radio" name='2' value='d'>2 Jam <br>
						<input type="radio" name='2' value='e'>Diatas dari 2 jam <br>
					</li>
					<li>
						Diwaktu manakah anda sering menggunakan hotspot Untan ? <br>
						<input type="radio" name='3' value='a'>08.00 &minus; 10.00 WIB<br>
						<input type="radio" name='3' value='b'>10.00 &minus; 12.00 WIB<br>
						<input type="radio" name='3' value='c'>12.00 &minus; 15.00 WIB<br>
						<input type="radio" name='3' value='d'>15.00 &minus; 18.00 WIB<br>
						<input type="radio" name='3' value='e'>18.00 &minus; 08.00 WIB<br>
					</li>
					<li>
						Apa yang membuat anda mengakses internet melalui hotspot Untan disamping hotspot lain yang ada di kampus ?<br>
						<input type="radio" name='4' value='a'>Gratis<br>
						<input type="radio" name='4' value='b'>Cepat<br>
						<input type="radio" name='4' value='c'>Ingin Akses Siakad<br>
						<input type="radio" name='4' value='d'>Mau tidak mau<br>
						<input type="radio" name='4' value='e'>Tidak ada pilihan lain<br>
					</li>
					<li>
						Ketika mengakses hotspot Untan, layanan internet apa yang sering anda akses ?<br>
						<input type="radio" name='5' value='a'>Jurnal Ilmiah<br>
						<input type="radio" name='5' value='b'>Searching Materi Pendidikan<br>
						<input type="radio" name='5' value='c'>Social Media<br>
						<input type="radio" name='5' value='d'>Youtube / Streaming Media<br>
						<input type="radio" name='5' value='e'>Game Online<br>
					</li>
					<li>
						Dengan layanan hotspot Untan saat ini, seperti login menggunakan login siakad, bagaimana menurut anda ? <br>
						<input type="radio" name='6' value='a'>Sangat Membantu<br>
						<input type="radio" name='6' value='b'>Membantu<br>
						<input type="radio" name='6' value='c'>Menyusahkan<br>
						<input type="radio" name='6' value='d'>Sangat Menyusahkan<br>
						<input type="radio" name='6' value='e'>Terserah<br>
					</li>
					<li>
						Menurut anda apakah hal yang paling menjengkelkan yang anda alami ketika anda mengakses hotspot Untan ?<br>
						<input type="radio" name='7' value='a'>Lambat<br>
						<input type="radio" name='7' value='b'>Sinyal tidak stabil<br>
						<input type="radio" name='7' value='c'>Banyak situs di blok<br>
						<input type="radio" name='7' value='d'>Tempat mengakses yang tidak memadai<br>
						<input type="radio" name='7' value='e'>Tidak responsif terhadap gangguan yang ada<br>
					</li>
					<li>
						Perbaikan apa yang anda sarankan untuk akses hotspot / internet Untan ?<br>
						<input type="radio" name='8' value='a'>Kecepatan Akses<br>
						<input type="radio" name='8' value='b'>Coverage Hotspot yang lebih luas<br>
						<input type="radio" name='8' value='c'>Sinyal Hotspot yang lebih kuat<br>
						<input type="radio" name='8' value='d'>Kebebasan lebih dalam mengakses konten di internet<br>
						<input type="radio" name='8' value='e'>Di tempat akses hotspot, disediakan terminal&minus;terminal listrik<br>
						<input type="radio" name='8' value='f'>Tidak ada yang perlu diperbaiki<br>
					</li>
					<li>
						Selain layanan Internet Standar, layanan apalagi yang anda butuhkan untuk memudahkan anda belajar dan mencari informasi melalui internet ?<br>
						<input type="radio" name='9' value='a'>Tempat Penyimpanan Online seperti dropbox dlsbnya<br>
						<input type="radio" name='9' value='b'>Download Konten Pendidikan Dipercepat<br>
						<input type="radio" name='9' value='c'>Komunikasi VOIP<br>
						<input type="radio" name='9' value='d'>Email Mahasiswa<br>
						<input type="radio" name='9' value='e'>Portal Mahasiswa<br>
					</li>
					<li>
						Setujukah anda jika Social Media Seperti Facebook dlsbnya di tutup ?<br>
						<input type="radio" name='10' value='a'>Setuju<br>
						<input type="radio" name='10' value='b'>Tidak<br>
					</li>
					<li>
						Setujukah anda jika Untan memiliki social media sendiri seperti facebook khusus untuk mahasiswa Untan ?<br>
						<input type="radio" name='11' value='a'>Setuju<br>
						<input type="radio" name='11' value='b'>Tidak<br>
					</li>
					<li>
						Setujukah anda. Jika layanan &minus; layanan streaming dan social media seperti youtube, facebook dlsbnya di tutup pada jam kerja dan dibuka lagi jam 18.00 &minus; 08.00 WIB ?<br>
						<input type="radio" name='12' value='a'>Setuju<br>
						<input type="radio" name='12' value='b'>Tidak<br>
					</li>
					<li>
						Mohon Kritik dan Saran Anda Untuk Kemajuan Hotspot UNTAN <br>
						<textarea name="pesan" id="pesan"></textarea>
					</li>
					<div id="error">
						Error message here 
					</div>
	
					<input type="hidden" name="username" value="<?php echo $username?>">
					<input type="hidden" name="password" value="<?php echo $password?>">
					<input type="hidden" name="hostname" value="<?php echo $hostname?>">
					<input type="hidden" name="status" value="<?php echo $status?>">

					<input type="hidden" id="no_1" name="no_1">
					<input type="hidden" id="no_2" name="no_2">
					<input type="hidden" id="no_3" name="no_3">
					<input type="hidden" id="no_4" name="no_4">
					<input type="hidden" id="no_5" name="no_5">
					<input type="hidden" id="no_6" name="no_6">
					<input type="hidden" id="no_7" name="no_7">
					<input type="hidden" id="no_8" name="no_8">
					<input type="hidden" id="no_9" name="no_9">
					<input type="hidden" id="no_10" name="no_10">
					<input type="hidden" id="no_11" name="no_11">
					<input type="hidden" id="no_12" name="no_12">
					</form>
					<input type="button" value="Selesai dan kirim kuesioner" id="submit">
				</ol>
		</div>
	</div>
</body>
</html>