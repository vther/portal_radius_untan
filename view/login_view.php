<?php	
	session_start();
	header("Cache-Control: no-cache, must-revalidate");
	if(isset($_REQUEST['hostname']) && !empty($_REQUEST['hostname']))
	{
		$hostname = $_REQUEST['hostname'];
		$_SESSION['hostname'] = $hostname;
	}
	else
	{
		$hostname = $_SESSION['hostname'];
	}
	$error = $_REQUEST['error'];
?>
<!doctype html>
<html>
<head>
	<title>Login Hotspot Untan</title>
	<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1.0, user-scalable=no">
	<link rel="icon" href="favicon.ico">
	<link rel="stylesheet" href="../css/new_login.css">
	<script>
		var link = '<?php echo $link; ?>';
	</script>
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript" src="../js/responsive.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			var error = $('#error').html().length;
			if(error == 0)
			{
				$('#error').hide();
			}

			$('#tombol_login').click(function(){
				window.open(link);
			});
			//menyimpan MAC address di input type hidden
			$(document).ready(function(){
				$('#mac').val(mac);
			});
		});
	</script>
</head>
<body>
	<div id="header">
		<img src="../image/header-untan.png" height="80px"> Hotspot Universitas Tanjungpura
	</div>
	<div id="page">
		<div id="left">
			<div id="login_form">
				<div id="login_form_header">
					Log In
				</div>
				<form action="login_proses.php" method="post">
					<input type="hidden" name="hostname" value="<?php echo $hostname ?>" />
					<input type="text" name="username" placeholder="Username"/><br>
					<input type="password" name="password" placeholder="Password"/><br>
					<input type="hidden" name="mac" id="mac" value="">
					<input type="submit" value="Log In" id="tombol_login:>
				</form>
			</div>
			<div id="error"><?php 
				if(!empty($error))
				{
					echo $error;
				}
			?></div>
		</div>
		<div id="right">
			<div id="panduan">
				<div id="panduan_header">
					Panduan Singkat Penggunaan Hotspot UNTAN
				</div>
				<div id="panduan_konten">
					<ol>
						<li>Gunakan Login dengan USER SIAKAD Anda</li>
						<li>Gunakan Peralatan Yang Sering ANda Gunakan Untuk Mengakses Hotspot</li>
						<li>Jika Username dan Password cocok, maka sistem akan mencatat MAC alat anda</li>
						<li>Untuk Next Login, Sistem akan mencocokkan antara MAC alat anda dengan Username dan Password Siakad Anda</li>
						<li>Anda Hanya Diperkenankan Untuk Mendaftarkan<strong> 3 alat yang berbeda</strong> milik anda untuk mengakses Hotspot Untan</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
