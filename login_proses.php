<?php 
	session_start();
	include('connection.php');

	$hostname = $_POST['hostname'];
	$username = $_POST['username'];
	$password = $_POST['password'];
	$mac = $_POST['mac'];

	if(preg_match('/^[0-9]{1,}$/', $username))
	{
		//periksa dulu apakah dosen atau karyawan
		$level = periksa_dosen_atau_pegawai($username);
		if($level=='dosen')
		{
			dosen($username, $password, $hostname, $mac);
		}
		else if($level=='karyawan')
		{
			header('Location:http://'.$hostname.'/login?username='.$username.'&password='.$password);
		}
		
		//dosen($username, $password, $hostname, $mac);
	}
	else if(is_numeric(substr($username, 1, strlen($username))))
	{
		mahasiswa($username, $password, $hostname, $mac);
	}
	else
	{
		header('Location:http://'.$hostname.'/login?username='.$username.'&password='.$password);
	}

	function periksa_dosen_atau_pegawai($username)
	{
		$query = mysql_query("SELECT srvid FROM rm_users WHERE username='$username'");
		if(mysql_num_rows($query)>0)
		{
			$row = mysql_fetch_assoc($query);
			if($row['srvid']=='7')
			{
				return 'karyawan';
			}
			else
			{
				return 'dosen';
			}
		}
		else
		{
			return 'dosen';
		}
	}

	function mahasiswa($username, $password, $hostname, $mac)
	{
		//memanggil file mahasiswa.php
		include('class/mahasiswa.php');
		$mahasiswa = new mahasiswa;
		$status = 'mahasiswa';
		$new_user = 0;
		//mengambil data dari service
		$mahasiswa->ambil_service($username, $password);
		if($mahasiswa->data['idmhs'] != 0)
		{
			//jika idmhs bukan 0 (menandakan username dan password benar)
			//mensinkronkan password di siakad dengan radius
			$mahasiswa->update_password($username);
			//kemudian periksa keaktifan mahasiswa
			if($mahasiswa->data['statakd']!=0)
			{
				//jika mahasiswa masih aktif, kemudian periksa apakah mahasiswa sudah terdaftar di radius
				if($mahasiswa->cek_radius($username) == 1)
				{
					//mengaktifkan akun mahasiswa (berjaga-jaga bila ada mahasiswa yang dari tidak aktif menjadi aktif)
					$mahasiswa->aktifkan($username);
					//periksa alat apakah menggunakan alat baru atau tidak?
					if($mahasiswa->periksa_alat($username, $mac) == 0)
					{	
						include_once('view/tambah_alat_view.php');
						/*
						//jika alat belum terdaftar, maka tambah alat
						if($mahasiswa->tambah_alat($username, $password, $mac) == 1)
						{
							//jika proses penambahan alat sukses, kemudian login
							$mahasiswa->login($username, $password, $hostname);
						}
						else
						{
							//jika proses gagal, tampilkan halaman login kembali dengan pesan error bahwa kuota alat sudah penuh
							session_unset();
							session_destroy();
							$mahasiswa->error($hostname, "akun anda telah terdaftar di perangkat lain");
						}
						*/
					}
					else
					{
						if($mahasiswa->periksa_nama_alat($username, $mac) == 0)
						{
							include_once('view/tambah_alat_view.php');
						}
						else if($mahasiswa->periksa_nama_alat($username, $mac) == 1)
						{
							$action = $mahasiswa->sudah_kuesioner($username);
							if($action == 'daftar_kuesioner')
							{
								//daftar kuesioner, kemudian login. isi kuesionernya kapan-kapan
								$mahasiswa->daftar_kuesioner($username, $password, $status);
								$mahasiswa->login($username, $password, $hostname);
							}
							else if($action == 'isi_kuesioner')
							{
								include_once('view/kuesioner_view.php');
							}
							else if($action == 'login')
							{
								$mahasiswa->login($username, $password, $hostname);
							}
							
						}
						/*
						//karena alat sudah terdaftar, maka login
						$mahasiswa->login($username, $password, $hostname);
						*/
					}
				}
				else
				{
					$new_user = 1;
					include_once('view/tambah_alat_view.php');
					/*
					//karena mahasiswa belum terdaftar di radius, maka daftarkan!
					$mahasiswa->tambah_mahasiswa($username, $password, $mac);
					$mahasiswa->login($username, $password, $hostname);
					*/
				}
			}
			else
			{
				//jika mahasiswa tidak aktif, maka nonaktifkan akun di radius
				$mahasiswa->nonaktifkan($username);
				//kemudian tampilkan pesan error bahwa mahasiswa tidak aktif
				$mahasiswa->error($hostname, "Akun anda sudah tidak aktif");
			}
		}
		else if(!isset($data['idmhs']))
		{
			if($mahasiswa->cek_radius($username) == 1)
			{
				if($mahasiswa->periksa_alat($username, $mac) == 0)
				{	
					include_once('view/tambah_alat_view.php');
				}
				else
				{
					$mahasiswa->login($username, $password, $hostname);
				}
			}
			else if($mahasiswa->cek_radius($username) == 0)
			{
				$mahasiswa->error($hostname, "Maaf, Kami tidak dapat mengambil data anda. Tampaknya SIAKAD sedang mengalami gangguan");
			}
		}
		else if($data['idmhs'] == 0)
		{
			//tampilkan pesan error bahwa username atau password salah
			$mahasiswa->error($hostname, "Username atau password salah");
		}
	}

	function dosen($username, $password, $hostname, $mac)
	{
		//memanggil file mahasiswa.php
		include('class/dosen.php');
		$status = 'dosen';
		$dosen = new dosen;
		$new_user = 0;
		//mengambil data dari service
		$dosen->ambil_service($username, $password);
		//kemudian periksa keaktifan dosen
		if($dosen->data['stat'] == 'aktif')
		{
			//mensinkronkan password di siakad dengan radius
			$dosen->update_password($username, $password);
			//jika dosen masih aktif, kemudian periksa apakah dosen sudah terdaftar di radius
			if($dosen->cek_radius($username) == 1)
			{
				//mengaktifkan akun dosen (berjaga-jaga bila ada dosen yang dari tidak aktif menjadi aktif)
				$dosen->aktifkan($username);
				//periksa alat apakah menggunakan alat baru atau tidak?
				if($dosen->periksa_alat($username, $mac) == 0)
				{
					include_once('view/tambah_alat_view.php');
					/*
					//jika alat belum terdaftar, maka tambah alat
					if($dosen->tambah_alat($username, $password, $mac) == 1)
					{
						//jika proses penambahan alat sukses, kemudian login
						$dosen->login($username, $password, $hostname);
					}
					else
					{
						//jika proses gagal, tampilkan halaman login kembali dengan pesan error bahwa kuota alat sudah penuh
						session_unset();
						session_destroy();
						$dosen->error($hostname, "akun anda telah terdaftar di perangkat lain");
					}
					*/
				}
				else
				{
					if($dosen->periksa_nama_alat($username, $mac) == 0)
					{
						include_once('view/tambah_alat_view.php');
					}
					else if($dosen->periksa_nama_alat($username, $mac) == 1)
					{
						//disini tempat kuesioner
						$dosen->login($username, $password, $hostname);
					}
					//karena alat sudah terdaftar, maka login
					//$dosen->login($username, $password, $hostname);
				}
			}
			else
			{
				$new_user = 1;
				include_once('view/tambah_alat_view.php');
				//karena dosen belum terdaftar di radius, maka daftarkan!
				//$dosen->tambah_dosen($username, $password, $mac);
				//$dosen->login($username, $password, $hostname);
			}
		}
		else if($dosen->data['stat'] == 'gagal')
		{
			$dosen->error($hostname, "Username dan password anda salah");
		}
		else
		{
			//untuk mencegah terjadinya gangguan akibat adanya error dalam siakad, 
			//maka, sistem akan melakukan uji coba login
			//$dosen->login($username, $password, $hostname);

			//jika dosen tidak aktif, maka nonaktifkan akun di radius
			//$dosen->nonaktifkan($username);
			//kemudian tampilkan pesan error bahwa dosen tidak aktif
			//$dosen->error($hostname, "Akun anda sudah tidak aktif");
		}		
	}
?>