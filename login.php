<?php
	session_start();
	if(isset($_SESSION['hostname']))
	{
		$hostname = $_SESSION['hostname'];
	}
	else if(isset($_REQUEST['hostname']) && strlen($_REQUEST['hostname']) > 0)
	{
		$hostname = $_REQUEST['hostname'];
		$_SESSION['hostname'] = $_REQUEST['hostname'];
	}
	$error = $_REQUEST['error'];

	$link = ambil_link_berita();

	function ambil_link_berita()
	{
		$html = file_get_contents('http://website.untan.ac.id/home');
		$explode = explode('<h2>Berita</h2>', $html);
		$html = $explode[1];
		$explode = explode("<a href='", $html);
		$link = explode("'", $explode[1]);
		$link = $link[0];

		return $link;
	}
?>
<!doctype html>
<html>
<head>
	<title>Login Hotspot Untan</title>
	<meta name="viewport" content="width=device-width, height=device-height, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1.0, user-scalable=no">
	<link rel="icon" href="favicon.ico">
	<?php 
		$angka = rand(0, 5);
		if($angka==0){?>
		<link rel="stylesheet" href="css/flat_login.css">
	<?php }else if($angka==1){ ?>
		<link rel="stylesheet" href="css/merah_login.css">
	<?php }else if($angka==2){ ?>
		<link rel="stylesheet" href="css/oren_login.css">
	<?php }else if($angka==3){ ?>
		<link rel="stylesheet" href="css/hijau_login.css">
	<?php }else if($angka==4){ ?>
		<link rel="stylesheet" href="css/biru_login.css">
	<?php }else if($angka==5){ ?>
		<link rel="stylesheet" href="css/ungu_login.css">
	<?php } ?>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/responsive.js"></script>
	<script type="text/javascript" src="http://<?php echo $hostname;?>/login_status.html"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			var link = '<?php echo $link; ?>';
			$('#trial').hide();
			if(trial == 'yes')
			{
				$('#trial').show();
				var link_trial = link_login_only+'?username=T-'+mac_esc;
				$('#link_trial').attr('href',link_trial);
			}
			var error = $('#error').html().length;
			if(error == 0)
			{
				$('#error').hide();
			}
			//menyimpan MAC address di input type hidden
			$('#mac').val(mac);
			$('#ip').html(ip);
			ambil_nama_nas(ip);

			$('#tombol_login').click(function(event) {
				window.open(link);
			});

			function ambil_nama_nas(ip)
			{
				$.post('ambil_nama_nas.php', {ip: ip}, function(data) {
					console.log(data);
				}, "json");
			}
		});
	</script>
</head>
<body>
	<div id="header_container">
		<div id="header">
			<img src="image/header-untan.png" height="80px"> Hotspot Universitas Tanjungpura
		</div>
	</div>
	<div id="page">
		<div id="left">
			<div id="login_form">
				<div id="login_form_header">
					Log In Hotspot
				</div>
				<form action="login_proses.php" method="post">
					<input type="hidden" name="hostname" value="<?php echo $hostname ?>" />
					<input type="text" name="username" placeholder="Username"/><br>
					<input type="password" name="password" placeholder="Password"/><br>
					<input type="hidden" name="mac" id="mac" value="">
					<input type="submit" value="Masuk" id="tombol_login">
				</form>
			</div>
			<div id="error"><?php 
				if(!empty($error))
				{
					echo $error;
				}
			?></div>
			<div id="trial">
				Anda tamu? silahkan login <a href="" id="link_trial">disini</a>
			</div>
			<!--<div id="info">
				Anda Login melalui <br> -nama NAS- <br>(<span id="ip"></span>)
			</div>-->
		</div>
		<div id="right">
			<div id="panduan">
				<div id="panduan_header">
					Panduan Singkat Penggunaan Hotspot UNTAN
				</div>
				<div id="panduan_konten">
					<ol>
						<li> Loginlah  dengan <strong>USER SIAKAD</strong> anda.</li>
						<li> Jika Username dan Password cocok, maka sistem secara otomatis mencatat identitas alat anda dan mendaftarkannya ke dalam sistem.</li>
						<li> Alat yang  telah terdaftarlah yang akan  diijinkan untuk mengakses Hotspot Untan.</li>
						<li> Proses pendaftaran peralatan anda dilakukan <strong>secara otomatis</strong> oleh sistem.</li>
						<li> Setiap kali anda login ke hotspot Untan, sistem akan mencocokan username anda dengan alat yg telah anda daftarkan.</li>
						<li> Untuk Mendaftarkan alat yang akan digunakan, cukup dengan <strong> melakukan login </strong> ke hotspot Untan.</li>
						<li> Anda hanya diperkenankan untuk mendaftarkan<strong> maksimal 3 alat </strong> milik anda untuk mengakses Hotspot Untan</li>
						<li> Oleh karenanya, gunakanlah peralatan yang sering anda gunakan untuk mengakses Hotspot Untan</li>
						<li> Selamat Menggunakan Akses Internet Untan, bantulah kami dengan menggunakannya secara bijak. Terimakasih</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
