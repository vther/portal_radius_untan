<?php
	session_start();
	if(isset($_SESSION['hostname']))
	{
		$hostname = $_SESSION['hostname'];
	}
	else if(isset($_REQUEST['hostname']) && strlen($_REQUEST['hostname']) > 0)
	{
		$hostname = $_REQUEST['hostname'];
		$_SESSION['hostname'] = $_REQUEST['hostname'];
	}
	$error = $_REQUEST['error'];
?>
<!doctype html>
<html>
	<head>
		<title>Login Hotspot Untan</title>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="http://<?php echo $hostname;?>/login_status.html"></script>
		<script>
			$(document).ready(function() {
				$('#mac').val(mac);
				setTimeout(function(){
					$('header').css('margin-top', '30px');
					$('#screen').delay(300).css('margin-top', '20px');

					setTimeout(function(){
						$('#container').attr('class', 'flip');
					}, 2500);
				}, 1000);
			});
		</script>
		<link rel="stylesheet" href="css/login_untan_femaledev.css">
	</head>
	<body>
		<header><img src="image/femaledev.png" alt=""></header>
		
		<div id="screen">
			<div id="container">
				<div id="depan">
					<div id="pesan_pink">Girls don't just wanna have fun,</div>
					<div id="pesan">but we create useful stuff and have fun while doing it!</div>
				</div>
				<div id="belakang">
					<form action="login_proses.php" method="post">
						<label for="">Username</label>
						<input type="text" name="username"><br>
						<label for="">Password</label>
						<input type="password" name="password"><br>
						<label for="">&nbsp;</label>
						<input type="submit" value="Have Fun">
						<input type="hidden" name="mac" id="mac" value="">
						<input type="hidden" name="hostname" value="<?php echo $hostname ?>" />
					</form>
				</div>
			</div>
		</div>

		<footer>
			<div id="footer_top">Supported By</div>
			<div id="footer_content">
				<img id="logo_kibar" src="image/logo-kibar.png" alt="kibar.co.id" title="kibar.co.id">
				<img id="logo_untan" src="image/header-untan.png" alt="Universitas Tanjungpura" title="Universitas Tanjungpura">
				<img id="logo_google" src="image/google.png" alt="">
			</div>
		</footer>
	</body>
</html>