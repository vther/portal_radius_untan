<?php 
	session_start();
	include('connection.php');

	$hostname = $_POST['hostname'];
	$username = mysql_real_escape_string($_POST['username']);
	$password = $_POST['password'];
	$mac = $_POST['mac'];
	$status = $_POST['status'];
	$nama_perangkat = $_POST['nama_perangkat'];
	$new_user = $_POST['new_user'];

	if($new_user == 0)
	{
		//tambah alat baru, kemudian login
		if($status == 'mahasiswa')
		{
			include('class/mahasiswa.php');
			$mahasiswa = new mahasiswa;
			$mahasiswa->ambil_service($username, $password);
			//lakukan proses penambahan alat
			if($mahasiswa->tambah_alat($username, $password, $mac, $nama_perangkat) == 1)
			{
				//jika proses penambahan alat sukses, kemudian login
				$mahasiswa->login($username, $password, $hostname);
			}
			else
			{
				//jika gagal, maka tampilkan pesan error
				session_unset();
				session_destroy();
				$mahasiswa->error($hostname, "akun anda telah terdaftar di perangkat lain");
			}
		}
		else if($status == 'dosen')
		{
			include('class/dosen.php');
			$dosen = new dosen;
			$dosen->ambil_service($username, $password);
			//lakukan proses penambahan alat
			if($dosen->tambah_alat($username, $password, $mac, $nama_perangkat) == 1)
			{
				//jika proses penambahan alat sukses, kemudian login
				$dosen->login($username, $password, $hostname);
			}
			else
			{
				//jika gagal, maka tampilkan pesan error
				session_unset();
				session_destroy();
				$dosen->error($hostname, "akun anda telah terdaftar di perangkat lain");
			}
		}
	}
	else if($new_user == 1)
	{
		//tambah user baru, kemudian login
		if($status == 'mahasiswa')
		{
			include('class/mahasiswa.php');
			$mahasiswa = new mahasiswa;
			$mahasiswa->ambil_service($username, $password);
			//lakukan proses penambahan alat
			$mahasiswa->tambah_mahasiswa($username, $password, $mac, $nama_perangkat);
			//kemudian login
			$mahasiswa->login($username, $password, $hostname);
		}
		else if($status == 'dosen')
		{
			include('class/dosen.php');
			$dosen = new dosen;
			$dosen->ambil_service($username, $password);
			//lakukan proses penambahan alat
			$dosen->tambah_dosen($username, $password, $mac, $nama_perangkat);
			//kemudian login
			$dosen->login($username, $password, $hostname);
		}
	}
?>