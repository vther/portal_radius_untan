<?php
	session_start();
	if(isset($_REQUEST['hostname']) && !empty($_REQUEST['hostname']))
	{
		$hostname = $_REQUEST['hostname'];
		$_SESSION['hostname'] = $hostname;
	}
	else
	{
		$hostname = $_SESSION['hostname'];
	}
	$error = $_REQUEST['error'];
?>
<html>
	<head>
		<title>Login Hotspot Untan</title>
		<link rel="icon" href="favicon.ico">
		<link rel="stylesheet" href="css/login.css">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery_cycle.js"></script>
		<script type="text/javascript" src="js/jquery_easing.js"></script>
		<script type="text/javascript" src="js/login_script.js"></script>
		<script type="text/javascript" src="http://<?php echo $hostname;?>/login_status.html"></script>
		<script type="text/javascript">
			//menyimpan MAC address di input type hidden
			$(document).ready(function(){
				$('#mac').val(mac);
			});
		</script>
	</head>
	<body>
		<div id="content">
			<div id="message">
				<img src="image/header-untan.png" height="80px"><br>
				Universitas Tanjungpura
			</div>
			<div id="form">
				<div id="form_header">
					Login
				</div>
				<form action="login_proses.php" method="post">
					<input type="hidden" name="hostname" value="<?php echo $hostname ?>" />
					<input type="text" name="username" placeholder="Username"/><br>
					<input type="password" name="password" placeholder="Password"/><br>
					<input type="hidden" name="mac" id="mac" value="">
					<input type="submit" value="Login">
				</form>	
			</div>
		</div>
		<div id="error">
		<?php 
			if(!empty($error))
			{
				echo $error;
			}
			else
			{
                        echo "<p align=center> PETUNJUK PENGGUNAAN </p>";
                        echo "<div align=left>1. Gunakan Login dengan USER SIAKAD Anda </div>";
                        echo "<div align=left>2. Gunakan Peralatan Yang Sering Anda Gunakan Untuk Mengakses Hotspot  </div>";
                        echo "<div align=left>3. Jika User dan Password cocok, maka sistem akan mencatat MAC alat anda  </div>";
                        echo "<div align=left>4. Untuk Next Login, Sistem akan mencocokkan antara MAC alat anda dengan Username dan Password Siakad Anda  </div>";
                        echo "<div align=left>5. Anda Hanya Diperkenankan Untuk Mendaftarkan <b>3 alat yang berbeda </b> milik anda untuk mengakses Hotspot Untan  </div>";
                        }
		?>
		</div>
	</body>
</html>
