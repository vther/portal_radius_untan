<?php 
	session_start();
	if(isset($_SESSION['hostname']))
	{
		$hostname = $_SESSION['hostname'];
	}
	else if(!empty($_REQUEST['hostname']))
	{
		$hostname = $_REQUEST['hostname'];
	}
?>
<html>
	<head>
		<script type="text/javascript" src="jquery.js"></script>
		<script type="text/javascript" src="http://<?php echo $hostname;?>/login_status.html"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				if(logged_in == "yes")
				{
					$('#logged_in').submit();
				}
				else
				{
					$('#login').submit();
				}
			});
		</script>
	</head>
	<body>
		<form action="logged_in.php" method="post" id="logged_in">
			<input type="hidden">
		</form>
		<form action="login.php?hostname=<?php echo $hostname;?>" method="post" id="login">
			<input type="hidden">
		</form>
	</body>
</html>