<?php 
class mahasiswa{

	public $service_id = 0;
	public $expiration = '2015-01-01';
	public $data = array();

	public function ambil_service($username, $password)
	{
		//menyesuaikan password
		$password = 'x'.$password;
		//ambil data dari service siakad
		$json = file_get_contents('http://203.24.50.30:8089/Datasnap/Rest/Tservermethods1/loginmhs/'.$username.'/'.$password);
		$data = json_decode($json, TRUE);
		foreach($data as $var)
		{
			$row = $var[0];
		}
		$this->data = $row;
	}

	public function cek_radius($username)
	{
		$query = mysql_query("SELECT id FROM rm_users WHERE username='$username'");
		if(mysql_num_rows($query) > 0)
		{
			return 1; 
		}
		else
		{
			return 0;
		}
	}

	public function tambah_mahasiswa($username, $password, $mac, $nama_alat)
	{
		$password = 'x'.$password;
		$pass_md5 = md5($password);
		$tanggal = date('Y-m-d');
		$firstname = $this->data['nama'];
		mysql_query("INSERT INTO rm_users (username, password, groupid, enableuser, mac, usemacauth, srvid, owner, expiration, createdon, createdby, firstname, device_name)
			VALUES('$username', '$pass_md5', '1', '1', '$mac', '1', '$this->service_id', 'admin', '$this->expiration', '$tanggal', 'admin', '$firstname', '$nama_alat')");
		//masukkan data kedalam tabel 'radcheck'
		mysql_query("INSERT INTO radcheck (username, attribute, op, value) 
			VALUES('$username', 'Cleartext-Password', ':=', '$password')");
		mysql_query("INSERT INTO radcheck (username, attribute, op, value) 
			VALUES('$username', 'Simultaneous-Use', ':=', '1')");
	}

	public function update_password($username)
	{
		$query = mysql_query("SELECT password FROM rm_users WHERE username='$username'");
		$password = mysql_result($query, 0);
		$password_siakad = $this->data['passwd'];
		$pass_md5_siakad = md5($password_siakad);
		//periksa apakah ada perubahan password?
		if($pass_md5_siakad != $password)
		{
			//jika ada perubahan, maka update password pada radius
			mysql_query("UPDATE rm_users SET password='$pass_md5_siakad' WHERE username='$username'");
			mysql_query("UPDATE radcheck SET value='$password_siakad' WHERE username='$username' AND op=':=' AND attribute='Cleartext-Password'");
		}
	}

	public function periksa_alat($username, $mac)
	{
		mysql_query("DELETE FROM rm_users WHERE username='$username' AND ( mac='' OR mac='[object HTMLInput')");
		//mysql_query("DELETE FROM rm_users WHERE username='$username' AND mac=''");
		$query = mysql_query("SELECT id FROM rm_users WHERE username='$username' AND mac='$mac'");
		//periksa apakah alat sudah terdaftar atau belum
		if(mysql_num_rows($query) == 0)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}

	public function nonaktifkan($username)
	{
		mysql_query("UPDATE rm_users SET enableuser='0' WHERE username='$username'");
	}

	public function aktifkan($username)
	{
		mysql_query("UPDATE rm_users SET enableuser='1' WHERE username='$username'");
	}

	public function tambah_alat($username, $password, $mac, $nama_alat)
	{
		mysql_query("DELETE FROM rm_users WHERE username='$username' AND device_name=''");
		$query = mysql_query("SELECT id FROM rm_users WHERE username='$username' ");
		//jika jumlah alat user dibawah 3
		if(mysql_num_rows($query) < 3)
		{
			$password = 'x'.$password;
			$pass_md5 = md5($password);
			$firstname = $this->data['nama'];
			$tanggal = date('Y-m-d');
			mysql_query("INSERT INTO rm_users (username, password, groupid, enableuser, mac, usemacauth, srvid, owner, expiration, createdon, createdby, firstname, device_name)
				VALUES('$username', '$pass_md5', '1', '1', '$mac', '1', '$this->service_id', 'admin', '$this->expiration', '$tanggal', 'admin', '$firstname', '$nama_alat')");
			return 1;
		}
		else
		{
			return 0;
		}
	}

	public function logout_acct($username)
	{
		//ambil id max dari tabel
		$query = mysql_query("SELECT MAX(radacctid) FROM radacct WHERE username='$username'");
		$max_id = mysql_result($query, 0);
		$timestamp = date('Y-m-d H:i:s');
		mysql_query("UPDATE radacct SET acctsessiontime=100, acctinputoctets=1000, acctoutputoctets=1000, acctterminatecause='User-Request', acctstoptime='$timestamp' WHERE radacctid='$max_id'");
	}

	public function login($username, $password, $hostname)
	{
		//$this->logout_acct($username);
		$_SESSION['username'] = $username;
		$_SESSION['password'] = $password;
		$password = 'x'.$password;
		$_SESSION['hostname'] = $hostname;
		$_SESSION['status'] = 'mahasiswa';
		mysql_query("UPDATE rm_users SET address='$hostname' WHERE username='$username'");
		header('Location:http://'.$hostname.'/login?username='.$username.'&password='.$password);
	}

	public function error($hostname, $error_message)
	{
		header('Location:http://login.untan.ac.id/portal/login.php?error='.$error_message);
	}

	public function periksa_nama_alat($username, $mac)
	{
		$query = mysql_query("SELECT id FROM rm_users WHERE username='$username' AND mac='$mac' AND device_name=''");
		//periksa apakah nama alat sudah ada atau belum
		if(mysql_num_rows($query) == 0)
		{	
			//jika tidak ada, maka sudah ada nama alat
			return 1;
		}
		else
		{
			//jika ada, maka tambah nama alat
			return 0;
		}
	}

	public function sudah_kuesioner($username)
	{
		$query = mysql_query("SELECT sudah_kuesioner FROM kuesioner WHERE username='$username'");
		if(mysql_num_rows($query) == 0)
		{
			return 'daftar_kuesioner';
		}
		else
		{
			if(mysql_result($query, 0) == 1)
			{
				return 'login';
			}
			else if(mysql_result($query, 0) == 0)
			{
				return 'isi_kuesioner';
			}
		}
	}

	public function daftar_kuesioner($username, $password, $status)
	{
		$angkatan = '20'.substr($username, 4, 5);
		$program_studi = $this->data['progdi'];
		$nama = $this->data['nama'];
		mysql_query("INSERT INTO kuesioner (username, program_studi, nama, angkatan, status) VALUES ('$username', '$program_studi', '$nama', '$angkatan', '$status')");
	}
}
?>