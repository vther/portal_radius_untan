var window_width = 0;
var pengurangan_sekarang = '';
$(document).ready(function() {
	resize_layar();
	$(window).resize(function(event) {
		resize_layar();
	});
});

function resize_layar()
{
	window_width = $(window).width();
	if(window_width<=999 && window_width>600)
	{
		if(pengurangan_sekarang!='besar')
		{
			pengurangan_sekarang = 'besar';
			$('#header img').css('margin-left', (window_width/2)-30+'px');
			var lebar_kotak = $('#login_form').width();
			lebar_kotak = lebar_kotak-40;
			$('#login_form, #panduan, #error').css('width', lebar_kotak+'px');
		}
	}
	else if(window_width<=600)
	{
		if(pengurangan_sekarang!='kecil')
		{
			pengurangan_sekarang = 'kecil';
			$('#header img').css('margin-left', (window_width/2)-30+'px');
			var lebar_kotak = $('#login_form').width();
			lebar_kotak = lebar_kotak-20;
			$('#login_form, #panduan, #error').css('width', lebar_kotak+'px');
		}
	}
}