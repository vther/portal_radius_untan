$(document).ready(function(){
	var window_width = $(window).width();
	$('#content').css('margin-left',(window_width-740)/2+'px' );


//====================================================================================
//Efek tombol MERAH di klik atau di tekan
//====================================================================================
	$('#logout').mousedown(function(){
		$(this).css('background-color', '#c40000');
		$(this).css('box-shadow', '0px 0px 3px #444');
		$(this).css('border', 'solid 1px #f24848');
	});
	$('#logout').keydown(function(){
		$(this).css('background-color', '#c40000');
		$(this).css('box-shadow', '0px 0px 3px #444');
		$(this).css('border', 'solid 1px #f24848');
	});
	$('#logout').mouseenter(function(){
		$(this).css('background-color', '#ff0707');
	});
//====================================================================================
//Efek tombol MERAH kembali semula
//====================================================================================
	$('#logout').mouseleave(function(){
		$(this).css('background-color', '#FF8484');
		$(this).css('box-shadow', '0px 4px 8px -4px #222');
		$(this).css('border', 'solid 1px #f24848');
	});
	$('#logout').mouseup(function(){
		$(this).css('background-color', '#ff0707');
		$(this).css('box-shadow', '0px 4px 8px -4px #222');
		$(this).css('border', 'solid 1px #f24848');
	});
	$('#logout').keyup(function(){
		$(this).css('background-color', '#ff0707');
		$(this).css('box-shadow', '0px 4px 8px -4px #222');
		$(this).css('border', 'solid 1px #f24848');
	});	
});