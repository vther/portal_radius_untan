$(document).ready(function(){
	$('#error').hide();

	$('input[type="radio"]').change(function(){
		var nomor = $(this).attr('name');
		var value = $(this).attr('value');
		$('#no_'+nomor).val(value);
	});

	$('#submit').click(function(){
		var no_1 = $('#no_1').val().length;
		var no_2 = $('#no_2').val().length;
		var no_3 = $('#no_3').val().length;
		var no_4 = $('#no_4').val().length;
		var no_5 = $('#no_5').val().length;
		var no_6 = $('#no_6').val().length;
		var no_7 = $('#no_7').val().length;
		var no_8 = $('#no_8').val().length;
		var no_9 = $('#no_9').val().length;
		var no_10 = $('#no_10').val().length;
		var no_11 = $('#no_11').val().length;
		var no_12 = $('#no_12').val().length;

		var hasil_kali = no_1*no_2*no_3*no_4*no_5*no_6*no_7*no_8*no_9*no_10*no_11*no_12;
		if(hasil_kali == 0)
		{
			$('#error').fadeOut(300);
			$('#error').html('Seluruh pertanyaan di kuesioner harus dijawab!');
			$('#error').fadeIn(300);
		}
		else if(hasil_kali != 0)
		{
			$('#error').fadeOut(300);
			$('#form_kuesioner').submit();
		}
	});
});