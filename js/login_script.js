$(document).ready(function(){
	var error = $('#error').html().length;

	if(error == 0)
	{
		$('#error').hide();
	}

	resize();
	$(window).resize(function(){
		resize();
	});

	$('#content').delay(3000).cycle({
		fx:'scrollUp',
		timeout:2500,
		nowrap:1,
		easing:'easeOutCirc'
	});

	function resize()
	{
		var window_height = $(window).height();
		$('#content').css('margin-top',(window_height - 330)/2 +'px');
	}
});