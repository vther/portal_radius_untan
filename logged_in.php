<?php 
	session_start();
	include('connection.php');
	if(isset($_SESSION['hostname']))
	{
		$hostname = $_SESSION['hostname'];
	}
	else if(!empty($_REQUEST['hostname']))
	{
		$hostname = $_REQUEST['hostname'];
	}
	if(isset($_SESSION['username'], $_SESSION['password'], $_SESSION['status']))
	{
		//jika mahasiswa, maka tampilkan profil mahasiswa
		if($_SESSION['status'] == 'mahasiswa')
		{
			//memanggil file mahasiswa.php
			include_once('class/mahasiswa.php');
			$mahasiswa = new mahasiswa;
			//mengambil data dari service
			$mahasiswa->ambil_service($_SESSION['username'], $_SESSION['password']);

			//memanggil file alat.php
			include_once('class/alat.php');
			$alat = new alat;
			//mengambil data alat
			$alat->ambil_alat($_SESSION['username']);
			$link = ambil_link_berita();
			include_once('view/logged_in_view_mahasiswa.php');
		}
		//jika dosen, maka tampilkan profil dosen
		else if($_SESSION['status'] == 'dosen')
		{
			//memanggil file dosen.php
			include_once('class/dosen.php');
			$dosen = new dosen;
			//mengambil data dari service

			//memanggil file alat.php
			include_once('class/alat.php');
			$alat = new alat;
			//mengambil data alat
			$alat->ambil_alat($_SESSION['username']);
			
			$dosen->ambil_service($_SESSION['username'], $_SESSION['password']);
			$link = ambil_link_berita();
			include_once('view/logged_in_view_dosen.php');
		}
	}
	else
	{
		echo '<a href="http://'.$hostname.'/logout">Log Out</a>';
	}


	function ambil_link_berita()
	{
		$html = file_get_contents('http://website.untan.ac.id/home');
		$explode = explode('<h2>Berita</h2>', $html);
		$html = $explode[1];
		$explode = explode("<a href='", $html);
		$link = explode("'", $explode[1]);
		$link = $link[0];

		return $link;
	}
?>